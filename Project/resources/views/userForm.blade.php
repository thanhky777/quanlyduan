<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Form</title>
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="{{ URL::asset('/css/user.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('/css/project.css') }}" />


</head>
<body>
<div class="sidebar">
        <div class="logo-details">
            <i class='bx bxl-c-plus-plus'></i>
            <span class="logo_name">Project</span>
        </div>
        <ul class="nav-links">
            <li>
                <a href="#">
                    <i class='bx bx-grid-alt' ></i>
                    <span class="link_name">Dashboard</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <i class='bx bxs-user-rectangle' ></i>
                    <span class="link_name">User</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <i class='bx bx-box' ></i>
                    <span class="link_name">Project</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <i class='bx bx-category'></i>
                    <span class="link_name">Category</span>
                </a>
            </li>

            <li>
                <a href="#">
                <i class='bx bx-task' ></i>
                    <span class="link_name">DetailTask</span>
                </a>
            </li>
            
            <li>
                <a href="#">
                    <i class='bx bx-log-out' ></i>
                    <span class="link_name">Log out</span>
                </a>
            </li>
        </ul>
    </div>
    
    <section class="home-section">
        <nav>
            <div class="sidebar-button">
                <i class='bx bx-menu sidebarBtn' ></i>
                <span class="dashboard">User</span>
            </div>
            <div class="search-box">
                <input type="text" placeholder="Search ...">
                <i class='bx bx-search-alt-2' ></i>
            </div>
            <div class="profile-details">
                <img src="https://cdn5.vectorstock.com/i/1000x1000/27/89/user-account-flat-icon-vector-14992789.jpg">
                <span class="admin_name">Account</span>
                <i class='bx bx-caret-down' ></i>
            </div>
        </nav>

        <!-- home - content -->
        <div class="user">
            <h3>Thêm người dùng</h3>
                <form action="">
                    
                        <input class="input-name" type="text" placeholder="Name">
                    
                        <input class="input-password" type="password" placeholder="Password">
                    
                        <input class="input-confirmation" type="text" placeholder="Password confirmation">
                    
                        <button class="btn-submit">Submit</button>
                </form>
            </div> 
    </section>
    <script >
        let sidebar = document.querySelector(".sidebar");
        let sidebarBtn = document.querySelector(".sidebarBtn");

        sidebarBtn.onclick = function() {
            sidebar.classList.toggle("active")
        }
    </script>
</body>
</html>