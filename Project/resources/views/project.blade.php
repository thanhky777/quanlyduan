<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reposive admin dashboard | CodingLap</title>
    <link rel="stylesheet" href="./css/project.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <!-- <link rel="stylesheet" href="./assests/css/reponsive.css"> -->
    <link rel="stylesheet" href="{{ URL::asset('/css/project.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('/css/reposive.css') }}" />
</head>
<body>
    <div class="sidebar">
        <div class="logo-details">
            <i class='bx bxl-c-plus-plus'></i>
            <span class="logo_name">Project</span>
        </div>
        <ul class="nav-links">
            <li>
                <a href="#">
                    <i class='bx bx-grid-alt' ></i>
                    <span class="link_name">Dashboard</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <i class='bx bxs-user-rectangle' ></i>
                    <span class="link_name">User</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <i class='bx bx-box' ></i>
                    <span class="link_name">Project</span>
                </a>
            </li>

            <li>
                <a href="#">
                    <i class='bx bx-category'></i>
                    <span class="link_name">Category</span>
                </a>
            </li>

            <li>
                <a href="#">
                <i class='bx bx-task' ></i>
                    <span class="link_name">DetailTask</span>
                </a>
            </li>
            
            <li>
                <a href="#">
                    <i class='bx bx-log-out' ></i>
                    <span class="link_name">Log out</span>
                </a>
            </li>
        </ul>
    </div>

    <section class="home-section">
        <nav>
            <div class="sidebar-button">
                <i class='bx bx-menu sidebarBtn' ></i>
                <span class="dashboard">Dashboard</span>
            </div>
            <div class="search-box">
                <input type="text" placeholder="Search ...">
                <i class='bx bx-search-alt-2' ></i>
            </div>
            <div class="profile-details">
                <img src="https://cdn5.vectorstock.com/i/1000x1000/27/89/user-account-flat-icon-vector-14992789.jpg">
                <span class="admin_name">Account</span>
                <i class='bx bx-caret-down' ></i>
            </div>
        </nav>

        <!-- home - content -->
        <!-- <div class="home-content"> 
            <div class="overview-boxes">
                <div class="box">
                    <div class="left-side">
                        <div class="box_topic"></div>
                        <div class="number">40,456</div>
                        <div class="indicator">
                            <i class='bx bx-up-arrow-alt' ></i>
                            <span class="text">Updated 10 minutes ago</span>
                        </div>
                    </div>
                    <i class='bx bx-cart-alt cart'></i>
                </div>
                <div class="box">
                    <div class="left-side">
                        <div class="box_topic">Total Sale</div>
                        <div class="number">33,746</div>
                        <div class="indicator">
                            <i class='bx bx-up-arrow-alt' ></i>
                            <span class="text">Updated 10 minutes ago</span>
                        </div>
                    </div>
                    <i class='bx bxs-cart-add cart two'></i>
                </div>
                <div class="box">
                    <div class="left-side">
                        <div class="box_topic">Total Profit</div>
                        <div class="number">25,143</div>
                        <div class="indicator">
                            <i class='bx bx-up-arrow-alt' ></i>
                            <span class="text">Updated 10 minutes ago</span>
                        </div>
                    </div>
                    <i class='bx bx-money cart three'></i>
                </div>
                <div class="box">
                    <div class="left-side">
                        <div class="box_topic">Order completed</div>
                        <div class="number">33,884</div>
                        <div class="indicator">
                            <i class='bx bx-up-arrow-alt' ></i>
                            <span class="text">Updated 10 minutes ago</span>
                        </div>
                    </div>
                    <i class='bx bx-list-check cart four' ></i>
                </div>
            </div>
        </div>  -->
    </section>
    <script >
        let sidebar = document.querySelector(".sidebar");
        let sidebarBtn = document.querySelector(".sidebarBtn");

        sidebarBtn.onclick = function() {
            sidebar.classList.toggle("active")
        }
    </script>
</body>
</html>